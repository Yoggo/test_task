package com.example.test_task.adapters;

import java.util.ArrayList;




import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.test_task.R;
import com.example.test_task.tools.JSONBlank;
/**
 * adapter for display list of sort results
 * */
public class JSONListAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private ArrayList<JSONBlank> jsonBlankList;

	public JSONListAdapter(Context activity, ArrayList<JSONBlank> jsonBlankList) {
		this.context = activity;
		this.jsonBlankList = jsonBlankList;
		layoutInflater = (LayoutInflater) this.context
	            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return jsonBlankList.size();
	}

	@Override
	public Object getItem(int position) {
		return jsonBlankList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		if(view == null){
			view = layoutInflater.inflate(R.layout.item_json_adapter, parent,false);
		}
		
		JSONBlank jsonBlank = jsonBlankList.get(position);
		 ((TextView)view.findViewById(R.id.item_json_adapter_key)).setText(jsonBlank.getKey());
		 ((TextView)view.findViewById(R.id.item_json_adapter_value)).setText(jsonBlank.getValue());
		return view;
	}

}
