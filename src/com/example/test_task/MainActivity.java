package com.example.test_task;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.test_task.adapters.JSONListAdapter;
import com.example.test_task.tools.JSONBlank;


public class MainActivity extends Activity {
	
	private String url;
	private ListView jsonListView;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        jsonListView = (ListView) findViewById(R.id.json_list_view);
        //address where we get json
        url = "http://echo.jsontest.com/key1/value1/key2/value2/key3/value3/key4/value4";
        //check, device is connected to network
        if(isNetworkConnected()){
        	 new GetJSON(url).execute();
        }else{
        	((TextView)findViewById(R.id.loading_text_view))
        	.setText(getResources().getString(R.string.no_network_connection));
        }
       
    }

    /**
     * class loader json at asynchronous stream
     * 
     * */
    
	private class GetJSON extends AsyncTask<Void, Void, Void> {
		
		private String url;
		private String response;

		GetJSON(String url) {
			this.url = url;
		}

		@Override
		protected Void doInBackground(Void... params) {
			HttpClient Client = new DefaultHttpClient();
			;
			try {
				HttpGet httpget = new HttpGet(url);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				response = Client.execute(httpget, responseHandler);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(response != null){
				try {
					//get json
					JSONObject jsonObject = new JSONObject(response);
					ArrayList<JSONBlank> jsonBlankList = new ArrayList<JSONBlank>();
					//get a list of key-value pairs
					jsonBlankList = getJsonBlankList(jsonObject);
				    JSONListAdapter jsonListAdapter = new JSONListAdapter(getApplicationContext(), jsonBlankList);
				    ((TextView)findViewById(R.id.loading_text_view)).setVisibility(View.GONE);
				    jsonListView.setAdapter(jsonListAdapter);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
		/**
		 * method to convert json in list of key-value pairs
		 * */
		private ArrayList<JSONBlank> getJsonBlankList(JSONObject jsonObject){
			Iterator<String> iter = jsonObject.keys();
			ArrayList<JSONBlank> jsonBlankArray = new ArrayList<JSONBlank>();
		    while (iter.hasNext()) {
		        try {
		        	String key = iter.next();
		            String value = jsonObject.getString(key);
		            JSONBlank jsonBlank = new JSONBlank();
		            jsonBlank.setKey(key);
		            jsonBlank.setValue(value);
		            jsonBlankArray.add(jsonBlank);
		        } catch (JSONException e) {
		            e.printStackTrace();
		        }
		    }
		    return jsonBlankArray;
		}
	}
	
	/**
	 * method for check network connection
	 * */
	private boolean isNetworkConnected() {
		  ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		  if (networkInfo == null) {
		   
		   return false;
		  } else
		   return true;
		 }
}
