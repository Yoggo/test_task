package com.example.test_task.tools;
/**
 * class for key-value pairs
 * */
public class JSONBlank {
	private String key;
	private String value;
	
	public void setKey(String key){
		this.key = key;
	}
	
	public void setValue(String value){
		this.value = value;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getValue(){
		return value;
	}
}
